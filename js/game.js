//change: 
//1 game need to take in player
//2 animation need to take in filename



function game(player){

	var _event_queue = [];
	var on_animation = 0;

	return {

		set_animation_mode: function(num){
			on_animation = num;
		},

		schedule_events: function (difficulty){
			var data = player.data;

			var timer =0;
			var sub_timer = 1;

			var instruction_set = [0];
			//doesn't work very well yet....
			// remove_all_element();
			on_animation =2;
			_event_queue = [];

			for(i=0; i<data.length; i++){
				var event = data[i][0].event;
				timer += data[i][1]/1000;
				sub_timer +=data[i][1]/1000;

				var channel = event.channel;
				if (event.subtype !== "noteOn" || MIDI.channels[channel].mute) {
					continue;
				}

				if(sub_timer !== 0){
					_event_queue.push(instruction_set);
					instruction_set = [];
					instruction_set.push(timer);
					sub_timer = 0;
				}		
				instruction_set.push(i);
			}

			_event_queue.splice(0,1);

			for(i=0; i<_event_queue.length; i++){
				var cur_instruction_set = _event_queue[i];
				var cur_delay_time = cur_instruction_set[0];
				var	sorted_instruction_set = this.sort(cur_instruction_set);

				var animation_set = [];
				var each = Math.floor(sorted_instruction_set.length/difficulty);
				var remainer = sorted_instruction_set.length % difficulty;

				for(j=0; j<difficulty; j++){
					animation_set[j] = each;
				}
				for(j=0; j<remainer; j++){
					animation_set[j] += 1;
				}

				var cur_counter = 0;
				var full_animation_set = [];
				for(j=0; j<difficulty; j++){
					var cur_end = cur_counter + animation_set[j];
					var cur_set = sorted_instruction_set.slice(cur_counter, cur_end);
					cur_counter = cur_end;
					var cur_animation = this.animation_object(cur_set, cur_delay_time);
					if(cur_animation !== null) full_animation_set.push(cur_animation);
				}

				_event_queue[i] = full_animation_set;
			}
		},

		//  add animation object
			//	1. get average of all nodes to get the position
			//	2. create the div and animation (oncomplete: TWEEN.pauseall() )
			//	3. create the object and return; 
		animation_object: function (instruction_set, delay_time,filename){
			root = MIDI.Player;
			var data = root.data;
			var sum =0;
			var num =instruction_set.length;
			if (num === 0) return null;
			for(k=0; k<num; k++){		
				var note = data[instruction_set[k]][0].event.noteNumber - (root.MIDIOffset || 0);
				sum += note;

			}
			// console.log(sum);
			//map the note to 26~102 (ignore 0~25, 103~126)
			var average_percentage = Math.min((((sum/num) -35) / 55), 0.999);
			var modified_percentage = Math.max(average_percentage, 0.001);
			var left = 1300 * modified_percentage;
			var output = this.create_img_element(left,filename);
			var tween = new TWEEN.Tween( { x: left, y: 100 } )
				            .to( { x: left, y: 500 }, 1000 )
				            .easing( TWEEN.Easing.Linear.None )
				            .onStart(function(){
				            	output.style.left = this.x + 'px';
				            	output.style.display = "block";
				            })
				            .onUpdate( function () {
				                output.style.top = this.y + 'px';
				            } )
				            .onComplete(function(){
				            	// output.style.display = "none";
				            	on_animation =0;
				            	// TWEEN.pauseAll();
				            })
				            .delay(delay_time*1000)
				            .start();

			return {
				instructions: instruction_set,
				element: output,
				animation: tween,
				x: left
			}
		},

		create_img_element: function(left, filename){
			var output = document.createElement('img');
			output.src = filename;
			output.style.cssText = 'position: absolute; left: 0px; top: 100px; font-size: 6px';
			output.style.left = left + 'px';
			output.style.display = "none";
			document.body.appendChild( output );
			return output;
		},

		//doesn't work very well
		remove_all_element: function(){
			for(var i=0; i<_event_queue.length; i++){
				for(var j=0; j<_event_queue[i].length; j++){
					var cur_obj = _event_queue[i][j];
					var cur_element = cur_obj.element;
					cur_element.parentNode.removeChild(cur_element);
					cur_obj.animation.stop();
				}
			}
		},

		sort: function (instruction_set){
			root = MIDI.Player;
			var data = root.data;

			instruction_set.splice(0,1);	
			var new_set = instruction_set;
			for(k=0; k<new_set.length; k++){
				var min_index = k;
				var min_value = data[new_set[k]][0].event.noteNumber;
				for(j=k; j<new_set.length; j++){
					if(data[new_set[j]][0].event.noteNumber < min_value){
						min_index= j;
						min_value =data[new_set[j]][0].event.noteNumber;
					}
				}
				var temp = new_set[k];
				new_set[k] = new_set[min_index];
				new_set[min_index] = temp;
			}
			return new_set;
		},

		animate:  function() {
			var m_animate = this.animate;
			if(on_animation === 1){
				// console.log("animate!");

				TWEEN.update();
				requestAnimationFrame(m_animate); // js/RequestAnimationFrame.js needs to be included too.
			}
			else if (on_animation === 0){
				TWEEN.pauseAll();
				cancelAnimationFrame(animate);
			}
			else{
				this.remove_all_element();
				cancelAnimationFrame(animate);
			}
		},

		trigger_sound: function(animation_obj){
			// console.log("event!");
			root = MIDI.Player;
			var data = root.data;

			var cur_set = animation_obj.instructions;

			for(var k=0; k<cur_set.length; k++ ){
				var event = data[cur_set[k]][0].event;

				var channel = event.channel;
				note = event.noteNumber - (root.MIDIOffset || 0);
				MIDI.noteOn(channel, event.noteNumber, event.velocity, 0);

				// document.getElementById("content").innerHTML += (note+"  ");

				var cur_index = cur_set[k] + 1;
				var cur_note = event.noteNumber;
				var delay_time = 0;
				while(true){
					delay_time += (data[cur_index][1])/1000;
					if(delay_time >2) break;
					
					var event = data[cur_index][0].event;
					var type = event.subtype;
					var notenumber = event.noteNumber;
					if(type === "noteOff" && notenumber === cur_note) {
						MIDI.noteOff(event.channel, event.noteNumber, delay_time);
						break;
					}
					cur_index++;
				}
			}
		},

		touch_handler: function(event) {
	// console.log(event.targetTouches);
	// player.start();
	// event.preventDefault();
	if(_event_queue.length !== 0){
		var cur_event = _event_queue[0];
		if(cur_event.length <= event.changedTouches.length) var number_hit = cur_event.length;
		else var number_hit = event.changedTouches.length;
		for(var i=0; i<number_hit; i++){
			var touch = event.changedTouches[i];
			var position = touch.pageX;
			var nearest_index = 0;
			var nearest_dist = 2000;
			for(var j=0; j<cur_event.length; j++){
				var cur_left = cur_event[j].x;
				if(Math.abs(cur_left-position) < nearest_dist){
					nearest_index = j;
					nearest_dist = Math.abs(cur_left-position);
				}
			}
			var cur_obj = cur_event[nearest_index];
			this.trigger_sound(cur_obj);
			cur_obj.animation.stop();
			cur_obj.element.style.display = "none";
			cur_event.splice(nearest_index,1);
		}
		// console.log("length " + cur_event.length);
		if(cur_event.length === 0){
			// console.log("on_animation" + on_animation);
			_event_queue.splice(0,1);
			TWEEN.resumeAll();

			if(on_animation !== 1){
				console.log("resume animation!");
				on_animation = 1;
				animate();
			}
		}

	}
}
	}
}